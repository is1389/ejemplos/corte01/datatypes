#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <stdbool.h>

int main(void){
	// Table header 
	printf("Data Type\tSize[B]\tMAX\t\t\tMIN\n");

	// bool
	// true/false is a int
	printf("bool\t\t%lu\t%d\t\t\t%d\n\n", sizeof(bool),true,false);

	// char
	// sizeof returns a unsigned long 
	printf("char\t\t%lu\t%d\t\t\t%d\n\n", sizeof(char),CHAR_MAX,CHAR_MIN);

	// unsgined char
	// U*_MIN: any unsigned type has a minimum value of 0 
	printf("unsigned char\t%lu\t%d\t\t\t%d\n\n",sizeof(unsigned char),
			UCHAR_MAX,0);

	// short int
	printf("short\t\t%lu\t%d\t\t\t%d\n\n", sizeof(short),
			SHRT_MAX, SHRT_MIN); 

	// unsigned short int
	printf("unsigned short\t%lu\t%d\t\t\t%d\n\n", sizeof(unsigned short),
			USHRT_MAX,0); 

	// int
	printf("int\t\t%lu\t%d\t\t%d\n\n", sizeof(int),INT_MAX, INT_MIN); 

	// unsigned int
	printf("unsigned int\t%lu\t%u\t\t%d\n\n", sizeof(unsigned int),
			UINT_MAX, 0); 

	// long int
	printf("long\t\t%lu\t%ld\t%ld\n\n", sizeof(long),LONG_MAX,LONG_MIN); 

	// unsigned long
	printf("unsigned long\t%lu\t%lu\t%d\n\n", sizeof(unsigned long),
			ULONG_MAX,0); 
	
	// float
	printf("float\t\t%lu\t%g\t\t%g\n\n", sizeof(float),FLT_MAX,FLT_MIN); 

	// doulble
	printf("double\t\t%lu\t%g\t\t%g\n\n", sizeof(double),DBL_MAX,DBL_MIN); 

	// long double
	printf("long double\t%lu\t%Lg\t\t%Lg\n\n", sizeof(long double),
			LDBL_MAX,LDBL_MIN); 

	return 0;
}
